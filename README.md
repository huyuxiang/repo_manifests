Repo Manifests
------

# 配置repo
## 自行下载repo
根据网络情况，从以下链接获取repo工具。
```
git clone https://gerrit.googlesource.com/git-repo  (谷歌官方源)
git clone https://mirrors.tuna.tsinghua.edu.cn/git/git-repo (国内清华源)
git clone https://gerrit-googlesource.lug.ustc.edu.cn/git-repo (国内中科大源)
```
## 配置REPO_URL
由于国内网络，需要修改REPO_URL，可参考以下修改：

  vim repo

```
## REPO_URL = 'https://gerrit-googlesource.proxy.ustclug.org/git-repo'
REPO_URL = 'https://mirrors.tuna.tsinghua.edu.cn/git/git-repo/'
```

## 使用配置好的repo（100ask）
```
https://git.dev.tencent.com/codebug8/repo.git
```
# 初始化仓库 repo init
## 迅为imx6ul开发板(全能版)
```
mkdir -p itop_imx6ul_sdk && cd itop_imx6ul_sdk
repo init -u https://gitee.com/gdky/repo_manifests -b master -m imx6ul/topeet_imx6ul_linux4.1.15_release.xml --no-repo-verify
```
## 工大科雅imx6ul主板(计量间)
获取源码

```
mkdir -p itop_imx6ull_sdk && cd itop_imx6ull_sdk
repo init -u https://gitee.com/gdky/repo_manifests -b master -m imx6ul/gdky_imx6ul_linux4.1.15_release.xml --no-repo-verify
```

# 同步代码repo sync

```
repo sync -j4

```
```
Fetching projects: 100% (3/3)
Fetching projects: 100% (3/3), done.
Checking out files: 100% (10246/10246), done.
Checking out files: 100% (56938/56938), done.es:  18% (10547/56938)
Syncing work tree: 100% (3/3), done
```
# 代码目录
查看下载好的源码目录
```
ls -l
```

